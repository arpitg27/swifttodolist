//
//  EditViewController.swift
//  ToDoSwift
//
//  Created by Arpit Gupta on 19/01/24.
//

import UIKit

class EditViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet var field:UITextField!
    var update:(()->Void)?
    var selectedIndex:IndexPath?
    var textInside:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        field.becomeFirstResponder()
        field.tintColor = UIColor.blue
        field.delegate=self
        field.text=textInside
       

        
        navigationItem.rightBarButtonItem=UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveTask))
        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        saveTask()
        return true
    }
    
    @objc func saveTask(){
        let vc = storyboard?.instantiateViewController(identifier: "view") as! ViewController
        vc.navigationItem.hidesBackButton = true
       
        guard let text = field.text, !text.isEmpty else{
            return
        }
        guard let row = selectedIndex  else {
           
            return
        }
       
   
      
        UserDefaults().setValue(text, forKey: "task_\(row.row)")
        update?()
        navigationController?.pushViewController(vc, animated: true)
    }

   

}
