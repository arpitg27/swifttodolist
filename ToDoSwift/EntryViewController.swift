

import UIKit

class EntryViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet var field:UITextField!
    
    var update:(()->Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        field.delegate=self
        field.becomeFirstResponder()
        field.tintColor = UIColor.blue
        navigationItem.rightBarButtonItem=UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveTask))
        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        saveTask()
        return true
    }
    @objc func saveTask(){
        guard let text = field.text, !text.isEmpty else{
            return
        }
        guard let count = UserDefaults().value(forKey: "count") as?Int else{
            return
        }
        let newCount = count+1
        UserDefaults().setValue(newCount, forKey: "count")
        UserDefaults().setValue(text, forKey: "task_\(newCount-1)")
        update?()
        navigationController?.popViewController(animated: true)
    }
    



}
