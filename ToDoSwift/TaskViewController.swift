

//  TaskViewController.swift
//  ToDoSwift
//
//  Created by Arpit Gupta on 19/01/24.
//
import UIKit
class TaskViewController: UIViewController{
   
    @IBOutlet var label:UILabel!
    @IBOutlet var button:UIButton!
    var update:(()->Void)?
    var selectedIndex:IndexPath?
    var task:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text=task
        navigationItem.rightBarButtonItem=UIBarButtonItem(title: "Delete ", style: .done, target: self, action: #selector(deleteTask))
        
    }
    @objc func deleteTask(){
        guard  let count = UserDefaults().value(forKey: "count") as? Int else{
            return
        }
       
        
        let newCount = count-1;
        UserDefaults().set(newCount, forKey: "count")
        
        guard let row = selectedIndex  else {
           
            return
        }
       
        for index in row.row..<count{
            
            UserDefaults().setValue(UserDefaults().value(forKey: "task_\(index+1)"), forKey: "task_\(index)")
        }
        
//
        update?()
        navigationController?.popViewController(animated: true)
        
     
    }
    @IBAction func didTapAdd(){
        
        let vc=storyboard?.instantiateViewController(identifier:"edit") as!EditViewController
        vc.title="Enter the task"
        vc.selectedIndex=selectedIndex
        vc.textInside=task
    
        navigationController?.pushViewController(vc, animated: true)
    }

}
